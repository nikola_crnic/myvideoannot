'use strict';

angular.module('videoApp.controllers')
.controller("contactCtrl", function($scope, $http, $location, VideoService)
{

  $scope.contact = {
    email: '',
    message: '',
    name: '',
    subject: ''
  };

  $scope.contactError = false;

  $scope.sendEmail = function(){
    VideoService.sendEmail($scope.contact).success(function(data, status, headers, config) {
      if(data == 'Failed'){
        $scope.contactError = true;
      }
      else if(data == 'Validation_failed')
        $scope.contactError = true;
      else{
        alert('Email sent successfully.');
        $location.path('/');
      }
    }); 
  }
});

