'use strict';

angular.module('videoApp.controllers', [])
.controller("playerCtrl", function($scope, $http, $filter, $routeParams, $modal, $rootScope, VideoService)
{
    //default values
    $scope.currentTime = 0;
    $scope.tagCategory = "";
    $scope.selectedTag = {};
    $scope.noAnnotations = false;

    //gets data for selected video
    VideoService.getVideos().success(function(data, status, headers, config) {
      $scope.selectedVideo = $filter('filter')(data, {id: $routeParams.id})[0];
    });

    //gets tag list
    VideoService.getTags().success(function(data, status, headers, config) {
      $scope.tags = data;
    });

    //gets tags for selected video by video id
    VideoService.getVideoTags($routeParams.id).success(function(data, status, headers, config) {
      $scope.videoTags = data;
      if(data.length == 0)
        $scope.noAnnotations = true;
    });

    //creates new modal instance with current video time and tag category.
    $scope.addCurrentTime = function(currentVideoTime, videoCat){
      if($rootScope.isAuthenticated){
        $scope.currentTime = currentVideoTime;
        $scope.tagCategory = videoCat;

        var modalInstance = $modal.open({
          animation: true,
          templateUrl: 'myModalContent.html',
          controller: 'ModalInstanceCtrl',
          resolve: {
            currentTime: function () {
              return $scope.currentTime;
            },
            videoCat: function () {
              return $scope.tagCategory;
            },
            videoID: function () {
              return $scope.selectedVideo.id;
            }, 
          }
        });
      }else{
        alert('Please log in to add video tag.');
      }
    };

    //gets selected tag data
    $scope.getSelectedTag = function(tag){
      $scope.selectedTag = tag;
    };

    //returns selected tag data to player directive
    $scope.goToTagTime = function(){
      return $scope.selectedTag;
    };
  })

.controller('ModalInstanceCtrl', function ($scope, $rootScope, $window, $modalInstance, currentTime, videoCat, videoID, VideoService) {
  $scope.tagCategory = videoCat;
  $scope.tagTime = currentTime;
  $scope.tagError = false;
  $scope.tagText = '';

//saves tag data to db
$scope.ok = function(){
  $scope.tag = {
   category: videoCat,
   title: $scope.tagText,
   videoID: videoID,
   user: $rootScope.currentUser.username,
   startTime: $scope.tagTime*1000,
   endTime: ($scope.tagTime*1000) + 2000
 };
 
   VideoService.saveTag($scope.tag).success(function(data, status, headers, config) {
    if(data == 'Failed'){
      $scope.tagError = true;
    }
    else{
      $modalInstance.close();
      $window.location.href = '/video/' + videoID;
    }
   });
};

//modal dismiss
$scope.cancel = function () {
  $modalInstance.dismiss('cancel');
};

});

