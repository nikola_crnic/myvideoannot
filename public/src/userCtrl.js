'use strict';

angular.module('videoApp.controllers')
.controller("userCtrl", function($scope, $http, $location, $rootScope, $cookieStore, $window, AuthService, AUTH_EVENTS)
{
    //default values
    $scope.user = {
        username: '',
        password: '',
        name: '',
        email: ''
    };

    $scope.credentials = {
        username: '',
        password: ''
    };

    $scope.userError = false;
    $scope.userExists = false;
    $scope.registerSuccess = false;
    $scope.registerFailed = false;

    //saves user data
    $scope.createUser = function(){
      AuthService.saveUser($scope.user).success(function(data, status, headers, config) {
        if(data == 'Created'){
          
          $scope.registerSuccess = true;
          $scope.userError = false;
          $scope.userExists = false;
          $scope.registerFailed = false;
        }
        else if(data == 'Register_failed'){
          
          $scope.registerSuccess = false;
          $scope.userError = false;
          $scope.userExists = false;
          $scope.registerFailed = true;
        }
        else if(data == 'Exists'){

          $scope.registerSuccess = false;
          $scope.userError = false;
          $scope.registerFailed = false;
          $scope.userExists = true;
        }
      });
    };

    //saves user extras
    $scope.saveUserExtras = function(){
      AuthService.saveExtras($rootScope.currentUser.username, $rootScope.extras).success(function(data, status, headers, config) {
        if(data == 'OK')
          $location.path('/');
        else
          $scope.registerFailed = false;
      });
    };

    //redirects to registration view
    $scope.register = function(){
      $location.path('/register');
    };

    //redirects to index page
    $scope.cancel = function(){
      $location.path('/');
    };

    //authenticates user with its credentials
    $scope.userAuth = function(){
      AuthService.authUser($scope.credentials).success(function(data, status, headers, config) {
            if(data == 'Invalid'){
              $scope.userError = true;
            }  
            else{
              $cookieStore.put('currentUser', {username: $scope.credentials.username, token: data});
              $window.location.href = '/';
            }
      });

      
    };
    
});
