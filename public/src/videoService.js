'use strict';

angular.module('videoApp.services', [])
.factory('VideoService', [
    '$http',
    function($http) {

        var URL = {
            GET_TAGS: 'data/tags.json',
            SAVE_TAG: '/addVideoTag',
            GET_VIDEO_TAGS: '/getVideoTags',
            GET_VIDEOS: 'data/resource.json',
            SEND_EMAIL: 'sendEmail'
        };

       //  //Get video data.
       //  var getVideo = function(video_id) {
       //     return $http.get(URL.GET_VIDEOS, 
       //                      { params: {
       //                          id: video_id
       //                      }
       //                  });
      	// };

        //Get videos.
        var getVideos = function() {
           return $http.get(URL.GET_VIDEOS);
        };

        //Get tags.
        var getTags = function() {

           return $http.get(URL.GET_TAGS);
        };

        var getVideoTags = function(videoID) {
           return $http.post(URL.GET_VIDEO_TAGS, {videoID: videoID});
        };

        //Save tag.
        var saveTag = function(tag) {
             return $http.post(URL.SAVE_TAG, {tag : tag});
        };

        //Send email.
        var sendEmail = function(contact) {
             return $http.post(URL.SEND_EMAIL, {contact : contact});
        };

        return {
            getVideoTags: getVideoTags,
            getVideos: getVideos,
            getTags: getTags,
            saveTag: saveTag,
            sendEmail: sendEmail
        };
    }
])







