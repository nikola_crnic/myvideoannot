'use strict';

angular.module('videoApp.ui.videojs', [])
.directive('videoloader', function () {
  var linker = function (scope, element, attrs){

    //predefined <video> attributes
    attrs.type = attrs.type || "video/mp4";
    attrs.id = "video-player";
    element.attr('id', attrs.id);

    var setup = {
      'techOrder' : ['html5', 'flash'],
      'controls' : true,
      'preload' : 'auto',
      'autoplay': 'true',
      'height' : 400
    };

    //initializes videojs player
    scope.$watch('videoloader', function(data){

      if(data != null){
        var videoData = scope.$eval(data);

        scope.$on('$destroy', function () {
          player.dispose();
        });

        var player = _V_(attrs.id, setup, function(){
          var source =([
            {type:"video/mp4", src: videoData.selectedVideo.path}
            ]);

          this.src({type : attrs.type, src: source });
        });

        //json array with video caption data
        var captionJson = [
        {
          "alignment": "C", 
          "data": "", 
          "startTime": -1, 
          "position": "HB", 
          "endTime": -1
        }];

        //if video contains tags, all tags are stored in captionJson array
        if (videoData.tags.length > 0) {
          angular.forEach(videoData.tags, function(value, key){
            var newCaption = {
              "alignment": "C", 
              "data": value.category + ": " + value.title, 
              "startTime": value.startTime, 
              "position": "HB", 
              "endTime": value.endTime
            };
            captionJson.push(newCaption);
          });
        };

        // var tagButton = angular.element(document.querySelector('.tagButton'));
        // tagButton.bind("click", function(e){
        //     scope.$parent.addCurrentTime(player.currentTime(), tagButton.text().replace(/ /g,''));
        // });

//calls function from parent controller to get video's current time  
$(".tagButton").click(function(){
  scope.$parent.addCurrentTime(player.currentTime(), $(this).text().replace(/ /g,''));
});

//calls function from parent controller to set video's current time
$(".btnTagTime").click(function(){
  var tag = scope.$parent.goToTagTime();
  player.currentTime(parseInt(tag.startTime)/1000);
});

player.caption({
 data: captionJson, 
 setting:{
  captionSize: 3,
  captionStyle : {
    'background-color': "#3074AE",
    'color':  'white',
    'padding': "10px"
  }
}
});
}
});

};
return {
  restrict : 'A',
  scope: {
    videoloader: "@"
  },
  link : linker
};
});
