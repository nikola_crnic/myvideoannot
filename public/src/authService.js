'use strict';

angular.module('videoApp.services')
.factory('AuthService', [
    '$http',
    function($http) {

        var URL = {
            AUTH_USER: '/authenticateUser',
            LOG_OUT: '/logOut',
            IS_AUTH: '/isAuthenticated',
            GET_USER: '/getUserData',
            SAVE_USER: '/createUser',
            SAVE_EXTRAS: '/saveUserData'
        };

        var authUser = function(credentials){
            return $http.post(URL.AUTH_USER, {userName : credentials.username, password : credentials.password});
            
        };

        var getUser = function(username){
            return $http.post(URL.GET_USER, {userName : username});
            
        };

        var saveUser = function(user) {
             return $http.post(URL.SAVE_USER, {user : user});
        };

        var saveExtras = function(username, extras) {
             return $http.post(URL.SAVE_EXTRAS, {userName: username, extras : extras});
        };

        var logOut = function(username){
             return $http.post(URL.LOG_OUT, {userName: username});
        };

        var isAuthenticated = function (token) {
          return $http.post(URL.IS_AUTH, {token: token});
        };

        return {
            authUser: authUser,
            logOut: logOut,
            isAuthenticated: isAuthenticated,
            getUser: getUser,
            saveUser: saveUser,
            saveExtras: saveExtras
        };
    }
]);







