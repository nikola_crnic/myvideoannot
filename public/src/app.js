'use strict';

angular.module('videoApp', ['videoApp.services', 'videoApp.controllers', 'videoApp.constants', 'videoApp.ui.videojs', 'ngRoute', 'ngCookies', 'ui.bootstrap'])
.config(function($routeProvider, $locationProvider){
	$routeProvider
	.when('/', {
		controller: 'listCtrl',
		templateUrl: 'views/list.html'
	})
	.when('/auth', {
		controller: 'userCtrl',
		templateUrl: 'views/auth.html'
	})
	.when('/register', {
		controller: 'userCtrl',
		templateUrl: 'views/register.html'
	})
	.when('/extras', {
		controller: 'userCtrl',
		templateUrl: 'views/extras.html'
	})
  .when('/contact', {
    controller: 'contactCtrl',
    templateUrl: 'views/contact.html'
  })
  .when('/privacy', {
    templateUrl: 'views/privacy.html'
  })
	.when('/video/:id', {
		controller: 'playerCtrl',
		templateUrl: 'views/player.html'
	});

	$locationProvider.html5Mode(true);
})

.controller('appCtrl', function ($scope, $rootScope, $window, $location, $cookieStore, AuthService, AUTH_EVENTS) {
	$rootScope.currentUser = $cookieStore.get('currentUser');
	$rootScope.extras = {
      name: '',
      email: ''
    }

  	//logs user out by its username
  	$scope.logOut = function(){
  		AuthService.logOut($scope.currentUser.username).success(function(data, status, headers, config) {
  			$cookieStore.remove('currentUser');
  			$window.location.href = '/auth';
  			//$location.path('/auth');
  		});
  	};

  	if($rootScope.currentUser != null){
  		//checks if user is authenticated by token
  		AuthService.isAuthenticated($rootScope.currentUser.token).success(function(data, status, headers, config) {
  			$rootScope.isAuthenticated = data;
  		});

  		//gets user data by its username
  		AuthService.getUser($rootScope.currentUser.username).success(function(data, status, headers, config) {
  			if(data == 'Error'){
  				$scope.userError = true;
  			}  
  			else{
  				$rootScope.extras = data;
  			}
  		});
  	}

});