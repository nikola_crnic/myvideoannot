'use strict';

angular.module('videoApp.constants', [])
.constant('AUTH_EVENTS', {
  registerSuccess: 'register-success',
  registerUserExists: 'register-user-exists',
  registerFailed: 'register-failed',
  loginSuccess: 'auth-login-success',
  loginFailed: 'auth-login-failed',
  logoutSuccess: 'auth-logout-success',
  sessionTimeout: 'auth-session-timeout',
  notAuthenticated: 'auth-not-authenticated',
  notAuthorized: 'auth-not-authorized'
});







