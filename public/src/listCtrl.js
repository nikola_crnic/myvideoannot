'use strict';

angular.module('videoApp.controllers')
.controller("listCtrl", function($scope, $http, VideoService)
{
 VideoService.getVideos().success(function(data) {
	 $scope.videos = data;
	});
 
});