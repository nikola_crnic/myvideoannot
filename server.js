var  express = require ('express'),
app = express(),
nodemailer = require('nodemailer'),
bodyParser = require("body-parser"),
mongojs = require('mongojs'),
databaseUrl = "mydb", // "username:password@example.com/mydb"
collections = ["videoAnnotations"],
db = mongojs(databaseUrl, collections);

UserManagement = require('user-management');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app

.use(express.static('./public'))
.get('*', function(req, res){
 res.sendfile('public/index.html');
})
.listen(3000);

//Create the reusable transport
var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'kraljshah@gmail.com',
        pass: 'splinter12'
    }
});
 
//Create the route that does all the magic when your contact form submit button is pressed
 
app.post('/sendEmail', function(req, res) {
            if(req.body.contact.email == '' || req.body.contact.subject == '' || req.body.contact.name == '' || req.body.contact.message == ''){
              res.send('Validation_failed');
            } else {
            // setup e-mail data with unicode symbols
            var mailOptions = {
                from: req.body.contact.email, // sender address
                to: 'kraljshah@gmail.com', // list of receivers. This is whoever you want to get the email when someone hits submit
                subject: req.body.contact.subject, // Subject line
                text: req.body.contact.message + '\n\n' + 'Sender: ' + req.body.contact.name + '\n\n' + 'You may contact this sender at: ' + req.body.contact.email // plaintext body
 
            };
 
            // send mail with defined transport object
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                    res.send('Failed');
                } else {
                    console.log('Message sent: ' + info.response);
                    res.send('Success');
                }
            });
            }
});

var users = new UserManagement();
users.load(function(err) {
  app.post('/createUser', function(req, res) {
    if(req.body.user.userName == '' || req.body.user.password == '' || req.body.user.name == '' || req.body.user.email == ''){
      res.send('Register_failed');
    } else {
      var EXTRAS = {
        name: req.body.user.name,
        email: req.body.user.email
      };

      console.log('Checking if the user exists');

      users.userExists(req.body.user.userName, function(err, exists) {

        if (exists) {
          console.log('  User already exists');
          res.send('Exists');
        } else {
          console.log('  User does not exist');
          console.log('Creating the user');
          users.createUser(req.body.user.userName, req.body.user.password, EXTRAS, function (err) {
            console.log('  User created');
            res.send('Created');
          });
        }

      });
    }
  });

  app.post('/authenticateUser', function(req, res) {
    users.authenticateUser(req.body.userName, req.body.password, function(err, result) {
      if (!result.userExists) {
        console.log('Invalid username');
        res.send('Invalid');
      } else if (!result.passwordsMatch) {
        console.log('Invalid password');
        res.send('Invalid');
      } else {
        res.send(result.token);
        console.log('User token is: '+ result.token);
      }
    });
  });

  app.post('/getUserData', function(req, res) {
    users.getExtrasForUsername(req.body.userName, function(err, extras) {
      if(extras != null){
        console.log('Name:' + extras.name);
        console.log(' Email:' + extras.email);
        res.send(extras);  
      }
      else{
        console.log('No extras for that username');
        res.send('Error');
      }

    });
  });

  app.post('/saveUserData', function(req, res) {
    if(req.body.extras.name == '' || req.body.extras.email == ''){
      res.send('Failed');
    } else {
      console.log(req.body.extras);
      users.setExtrasForUsername(req.body.userName, req.body.extras, function(err) {
        console.log('Extras saved');
        res.send('OK');  
      });
    }
  });

  app.post('/logOut', function(req, res) {
    users.getTokenForUsername(req.body.userName, function(err, token) {
      console.log('The user\'s token is: ' + token);
      users.expireToken(token, function(err) {
        res.send(token);
        console.log('User token: '+ token + 'is expired');
      });
    });
  });

  app.post('/isAuthenticated', function(req, res) {
    users.isTokenValid(req.body.token, function(err, valid) {
      if (!valid) {
        console.log('The token is not valid');
      } else {
        console.log('The token is valid');
      }
      res.send(valid);
    });
  });
});

app.post('/addVideoTag', function(req, res) {
  console.log(req.body.tag);
  if(req.body.tag.title == '' || req.body.tag.startTime == '' || req.body.tag.endTime == ''){
    console.log('Failed');
    res.send('Failed');
  } else {
    db.videoAnnotations.save({category: req.body.tag.category, title: req.body.tag.title, videoID: parseInt(req.body.tag.videoID), startTime: parseInt(req.body.tag.startTime),
      endTime: parseInt(req.body.tag.endTime), user: req.body.tag.user}, function(err, saved) {
        if( err || !saved ) console.log("Tag not saved");
        else{
          console.log("Tag saved");
          res.send('Saved');
        } 
    });
  }
});

app.post('/getVideoTags', function(req, res) {
  var query = {'videoID': parseInt(req.body.videoID)};
  db.videoAnnotations.find(query, function(err, videoAnnotations) {
    res.send(videoAnnotations);
  });
});
